import state from "./state";
import getters from "./getters";
import mutations from "./mutations";
import actions from "./actions";

export default config => {
  return {
    namespaced: true,
    state: state(config),
    getters,
    mutations,
    actions
  };
};
