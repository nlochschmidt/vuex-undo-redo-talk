import { deepCopy } from "@/utils/deepCopy";
import { calculateNextGroupMutationCount } from "@/store/undo/mutations";

const undo = ({ state, commit }, { namespace }) => {
  const doneStack = state.namespaces[namespace].done;
  if (doneStack.length > 0) {
    commit("stopRecording", { namespace });
    commit("undone", { namespace });
    const baseState = state.namespaces[namespace].baseState;
    commit(
      `${namespace}/_undoPlugin_restoreBaseState`,
      { baseState: deepCopy(baseState) },
      { root: true }
    );
    doneStack.forEach(mutation => {
      commit(mutation.type, deepCopy(mutation.payload), { root: true });
    });
    commit("startRecording", { namespace });
  }
};

const redo = ({ state, commit }, { namespace }) => {
  const redoStack = state.namespaces[namespace].undone;
  if (redoStack.length > 0) {
    commit("stopRecording", { namespace });
    const redoMutations = redoStack.slice(
      -calculateNextGroupMutationCount(redoStack)
    );
    redoMutations.reverse();
    redoMutations.forEach(mutation => {
      commit(mutation.type, deepCopy(mutation.payload), { root: true });
    });
    commit("redone", { namespace });
    commit("startRecording", { namespace });
  }
};

export default {
  undo,
  redo
};
