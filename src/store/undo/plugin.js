import undo from "@/store/undo";
import { deepCopy } from "@/utils/deepCopy";
import { uuidv4 } from "@/utils/createUUID";

const mutationListener = ({ commit }) => (mutation, state) => {
  // turns todoList/addTodo into ["todoList", "addTodo"]
  const [namespace, mutationType] = splitLastPathSegment(mutation.type);
  const undoState = getUndoState(state, namespace);
  const mutationShouldBeRecorded =
    undoState &&
    undoState.recording &&
    !undoState.ignoredMutations.includes(mutationType);

  if (mutationShouldBeRecorded) {
    if (isResetMutation(undoState, mutationType)) {
      const moduleState = copyModuleState(namespace, state);
      recordBaseState(commit, undoState, namespace, moduleState);
    } else {
      recordMutation(commit, namespace, deepCopy(mutation));
    }
  }
};

// Helper methods:

const extractState = (namespace, state) => {
  const path = namespace.split("/");
  return path
    .filter(el => el !== "")
    .reduce(
      (subState, property) =>
        subState && subState[property] ? subState[property] : null,
      state
    );
};

const isResetMutation = (undoState, localType) =>
  undoState.resetMutations.includes(localType);

const recordBaseState = (commit, undoState, namespace, baseState) => {
  commit("undo/recordBaseState", {
    namespace,
    baseState: undoState.cleanUpBaseState(baseState)
  });
};

const recordMutation = (commit, namespace, mutation) =>
  commit("undo/recordMutation", { namespace, mutation });

const splitLastPathSegment = path => {
  const lastSlashIndex = path.lastIndexOf("/");
  if (lastSlashIndex === -1) {
    return ["", path];
  } else {
    return [path.slice(0, lastSlashIndex), path.slice(lastSlashIndex + 1)];
  }
};

function getUndoState(state, namespace) {
  return state.undo.namespaces[namespace];
}

function copyModuleState(namespace, state) {
  return deepCopy(extractState(namespace, state));
}

const restoreBaseStateMutation = (state, { baseState }) => {
  Object.assign(state, baseState);
};

// Mutations that need to be added to the tracked module

export const undoMutations = {
  _undoPlugin_restoreBaseState: restoreBaseStateMutation
};

export const createUndoGroup = originalCommit => {
  const mutationGroupId = uuidv4();
  return {
    commit(typeOrMutationObject, payloadOrOptions, options) {
      // We need to check whether an Vuex object-style mutation is used to set the actionGroup correctly
      // See https://vuex.vuejs.org/guide/mutations.html
      const isObjectStyleMutation =
        typeof typeOrMutationObject === "object" && typeOrMutationObject.type;
      const payload = isObjectStyleMutation
        ? typeOrMutationObject
        : payloadOrOptions;
      payload._undoPlugin_mutationGroupId = mutationGroupId;

      originalCommit(typeOrMutationObject, payloadOrOptions, options);
    }
  };
};

export default config => store => {
  store.registerModule("undo", undo(config));
  store.subscribe(mutationListener(store));
};
