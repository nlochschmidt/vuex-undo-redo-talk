const canUndo = state => namespace => {
  return state.namespaces[namespace].done.length !== 0;
};

const canRedo = state => namespace => {
  return state.namespaces[namespace].undone.length !== 0;
};

export default {
  canUndo,
  canRedo
};
