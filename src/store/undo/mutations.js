const recordBaseState = (state, { namespace, baseState }) => {
  const undoState = state.namespaces[namespace];
  undoState.baseState = baseState;
  undoState.done = [];
  undoState.undone = [];
};

const recordMutation = (state, { namespace, mutation }) => {
  const undoState = state.namespaces[namespace];
  undoState.done.push(mutation);
  undoState.undone = [];
};

const stopRecording = (state, { namespace }) => {
  state.namespaces[namespace].recording = false;
};

const startRecording = (state, { namespace }) => {
  state.namespaces[namespace].recording = true;
};

const undone = (state, { namespace }) => {
  const undoState = state.namespaces[namespace];
  const undoGroupSize = calculateNextGroupMutationCount(undoState.done);
  if (undoGroupSize > 0) {
    const undoneMutations = undoState.done.splice(-undoGroupSize);
    undoneMutations.reverse();
    undoState.undone.push(...undoneMutations);
  }
};

const redone = (state, { namespace }) => {
  const undoState = state.namespaces[namespace];
  const redoGroupSize = calculateNextGroupMutationCount(undoState.undone);
  if (redoGroupSize > 0) {
    const redoneMutations = undoState.undone.splice(-redoGroupSize);
    redoneMutations.reverse();
    undoState.done.push(...redoneMutations);
  }
};

export const calculateNextGroupMutationCount = stack => {
  if (stack.length === 0) return 0;
  const lastIndex = stack.length - 1;
  const mutationGroupId = stack[lastIndex].payload._undoPlugin_mutationGroupId;
  if (!mutationGroupId) return 1;
  let undoGroupSize = 1;
  for (let index = lastIndex - 1; index >= 0; index--) {
    if (stack[index].payload._undoPlugin_mutationGroupId === mutationGroupId) {
      undoGroupSize++;
    } else {
      break;
    }
  }
  return undoGroupSize;
};

export default {
  recordBaseState,
  recordMutation,
  startRecording,
  stopRecording,
  undone,
  redone
};
