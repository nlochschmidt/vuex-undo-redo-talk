export default config => {
  return {
    namespaces: config.reduce((map, fullConfig) => {
      const { namespace, ...namespaceConfig } = fullConfig;
      map[namespace] = {
        baseState: null,
        done: [],
        undone: [],
        ignoredMutations: [],
        cleanUpBaseState: state => state,
        recording: true,
        ...namespaceConfig
      };
      return map;
    }, {})
  };
};
