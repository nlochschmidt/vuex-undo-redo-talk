import Vue from "vue";
import Vuex from "vuex";

import todoList from "./todo-list";
import undoPlugin from "./undo/plugin";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    todoList
  },
  plugins: [
    undoPlugin([
      {
        namespace: "todoList",
        resetMutations: ["setTodos"],
        ignoredMutations: ["setVisibility"],
        cleanUpBaseState: state => {
          delete state.visibility;
          return state;
        }
      }
    ])
  ],
  strict: process.env.NODE_ENV !== "production"
});
