import Vue from "vue";
import Vuex from "vuex";

import getters from "./getters";
import mutations from "./mutations";
import actions from "./actions";
import { undoMutations } from "@/store/undo/plugin";

Vue.use(Vuex);

export default {
  namespaced: true,
  state: {
    visibility: "all",
    todos: {}
  },
  getters,
  mutations: { ...mutations, ...undoMutations },
  actions
};
