import todoStorage from "./todoStorage";
import filters from "@/store/todo-list/filters";
import { createUndoGroup } from "@/store/undo/plugin";

const load = ({ commit }) => {
  commit("setTodos", { todos: todoStorage.fetch() });
};

const save = ({ state }) => {
  todoStorage.save(state.todos);
};

const addTodo = ({ commit }, title) => {
  if (!title) {
    return;
  }
  commit("addTodo", {
    todo: {
      id: todoStorage.uid++,
      title,
      completed: false
    }
  });
};

const updateTodoTitle = ({ commit, state }, { id, newTitle }) => {
  if (state.todos[id]) {
    commit("updateTodoTitle", { id, newTitle });
  }
};

const completeTodo = ({ commit, state }, id) => {
  if (!state.todos[id].completed) {
    commit("completeTodo", { id });
  }
};

const reopenTodo = ({ commit, state }, id) => {
  if (state.todos[id].completed) {
    commit("reopenTodo", { id });
  }
};

const removeTodo = ({ commit, state }, id) => {
  if (state.todos[id]) {
    commit("removeTodo", { id });
  }
};

const setVisibility = ({ commit, state }, newVisibility) => {
  if (state.visibility !== newVisibility) {
    commit("setVisibility", { newVisibility });
  }
};

const clearCompleted = ({ commit, getters }) => {
  const undoGroup = createUndoGroup(commit);

  const todosToDelete = filters
    .completed(getters["todos"])
    .map(todo => todo.id);

  todosToDelete.forEach(id => {
    undoGroup.commit("removeTodo", { id });
  });
};

const setAllCompleted = ({ commit, getters }, completed) => {
  const undoGroup = createUndoGroup(commit);
  getters["todos"].forEach(todo => {
    if (todo.completed !== completed) {
      if (completed) {
        undoGroup.commit("completeTodo", { id: todo.id });
      } else {
        undoGroup.commit("reopenTodo", { id: todo.id });
      }
    }
  });
};

export default {
  save,
  load,
  addTodo,
  updateTodoTitle,
  completeTodo,
  reopenTodo,
  removeTodo,
  clearCompleted,
  setAllCompleted,
  setVisibility
};
