import Vue from "vue";

const setTodos = (state, { todos }) => {
  Vue.set(state, "todos", todos);
};

const addTodo = (state, { todo }) => {
  Vue.set(state.todos, todo.id, todo);
};

const completeTodo = (state, { id }) => {
  state.todos[id].completed = true;
};

const reopenTodo = (state, { id }) => {
  state.todos[id].completed = false;
};

const updateTodoTitle = (state, { id, newTitle }) => {
  state.todos[id].title = newTitle;
};

const removeTodo = (state, { id }) => {
  Vue.delete(state.todos, id);
};

const setVisibility = (state, { newVisibility }) => {
  state.visibility = newVisibility;
};

export default {
  setTodos,
  addTodo,
  removeTodo,
  updateTodoTitle,
  completeTodo,
  reopenTodo,
  setVisibility
};
