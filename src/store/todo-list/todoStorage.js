// localStorage persistence
const STORAGE_KEY = "todos";
const todoStorage = {
  uid: 0,
  fetch() {
    const todos = JSON.parse(localStorage.getItem(STORAGE_KEY) || "[]");
    todos.forEach(function(todo, index) {
      todo.id = index;
    });
    todoStorage.uid = Object.values(todos).length;
    return todos.reduce((obj, todo) => {
      obj[todo.id] = todo;
      return obj;
    }, {});
  },
  save(todos) {
    localStorage.setItem(STORAGE_KEY, JSON.stringify(Object.values(todos)));
  }
};

export default todoStorage;
