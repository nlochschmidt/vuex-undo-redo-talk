import filters from "./filters";

const todosArray = state => {
  return Object.values(state.todos);
};

const filteredTodos = state => {
  return filters[state.visibility](todosArray(state));
};

const remaining = state => {
  return filters.active(todosArray(state)).length;
};

const allDone = state => {
  return remaining(state) === 0;
};

const visibility = state => {
  return state.visibility;
};

export default {
  visibility,
  todos: todosArray,
  filteredTodos,
  remaining,
  allDone
};
