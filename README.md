# TodoMVC with Undo-support using Vuex

[![Netlify Status](https://api.netlify.com/api/v1/badges/b0cf1bd1-0d9b-44df-9b72-fa74a13e242b/deploy-status)](https://app.netlify.com/sites/vuex-undo-redo-talk/deploys)

The code in this repository is part of a talk I presented on 2019-02-07 at the VueJSFrankfurt Meetup.

The slides for the talk can be found here: https://speakerdeck.com/nlochschmidt/next-level-vuex

Branches

- `demo1`: Plain TodoMVC implementation with Vuex but no Undo [Online](https://demo1--vuex-undo-redo-talk.netlify.com/)
- `demo2`: State-based undo implementation with an intentional bug (missing deepcopy) [Online](https://demo2--vuex-undo-redo-talk.netlify.com/)
- `demo2.1`: Actually working state-based undo [Online](https://demo2-1--vuex-undo-redo-talk.netlify.com/)
- `demo3`: Simple implementation of base state + forward mutation replaying [Online](https://demo3--vuex-undo-redo-talk.netlify.com/)
- `demo3.1`: Like demo3 but enhanced with ability to ignore some mutations [Online](https://demo3-1--vuex-undo-redo-talk.netlify.com/)
- `demo3.2`: Like demo3.1 but can group mutations together [Online](https://demo3-2--vuex-undo-redo-talk.netlify.com/)

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
```
yarn run build
```

### Run your tests
```
yarn run test
```

### Lints and fixes files
```
yarn run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
